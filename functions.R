################################################################################
###                                                                          ###
###  Title       : A Collection of Useful R Functions                        ###
###  Author      : Jo-fai Chow (or otherwise referenced)                     ###
###                                                                          ###
################################################################################


## =============================================================================
## autoload - Automatically install and load missing packages even if unavaiable
## Example: autoload(c("abc","abctools"))
##
## =============================================================================

autoload <- function(pkgs) {
  options(warn=-1)
  for (i in 1:length(pkgs)) {
    if (!library(pkgs[i], character.only=T, warn.conflicts=F, 
                 logical.return=T, quietly=T, verbose=F)) {
      cat("Installing Package {", pkgs[i], "} ...\n")
      install.packages(pkgs[1])
    } 
    cat("Loading Package {", pkgs[i], "} ...\n")
    library(pkgs[i], character.only=T, quietly=T, 
            warn.conflicts=F, verbose=F)
  }
  blank(1000)
  cat(length(pkgs), "packages installed/loaded.\n")
}

################################################################################

###  Function(s) : qLoad
###  Description : To load R packages quietly
###  Inputs      : A list of packages to be loaded
###  Outputs     : None

qLoad <- function(ls.packages) {
  suppressMessages(lapply(ls.packages, require, character.only=TRUE, 
                          quietly=TRUE, warn.conflicts=FALSE))
}

################################################################################

###  Function(s) : range01, range11, normalX
###  Description : Re-scale / Normalise Variables
###  Inputs      : matrix
###  Outputs     : matrix

### Re-scale between 0 and 1
range01 <- function(x){(x-min(x))/(max(x)-min(x))}

### Re-scale between -1 and 1
range11 <- function(x){(2*(x-min(x)) / (max(x)-min(x))) -1}

### Normalise a m-by-n matrix
normalX <- function(data, type=1) {
  ifelse(type==1,
         outputs <- apply(data, 2, range11),
         outputs <- apply(data, 2, range01))
  return(outputs)
}

################################################################################

###  Function(s) : timerStart, timerStop
###  Description : Stopwatch
###  Inputs      : N/A
###  Outputs     : time in seconds

### Start a timer
timerStart <- function() {
  return(proc.time())
}

### Stop a timer
timerStop <- function(timeStart) {
  time <- proc.time() - timeStart
  time <- time[1] + time[2] + time[3]
  return(time)
}

################################################################################

###  Functions   : blank
###  Description : Print blank lines to clear consolde
###  Inputs      : integer (number of blank lines to print)
###  Outputs     : N/A

blank <- function(nLines=100) {for (i in 1:nLines) {cat("\n")}}

################################################################################

###  Function(s) : openCore
###  Description : Active multiple cores for parallel computing
###  Inputs      : integer (number of cores)
###  Outputs     : N/A

openCore <- function(nCores=4) {
  
  autoload("doParallel")
  autoload("foreach")  
  cat("Activaing parallel processing ... ")
  cl <- makeCluster(nCores, type="SOCK")
  registerDoParallel(cl)
  cat(nCores,"cores have been successfully activated.\n")
}

################################################################################

###  Function(s) : Mode
###  Description : To find the mode of a matrix
###  Inputs      : x ... a m.n matrix
###  Outputs     : ux ... a single column matrix with the mode of each row
###  Source(s)   : http://goo.gl/8QvhS

Mode <- function(x) {
  ux <- unique(x)
  ux[which.max(tabulate(match(x, ux)))]
}

################################################################################

###  Function(s) : getGADM
###  Description : To download and combine gadm data from gadm.org
###  Inputs      : Country codes and map detail level
###  Outputs     : spdf (SpatialPolygonDataFrame)
###  Source(s)   : http://goo.gl/fRgb2
###  Usage       : spdf <- getGADM(c("ARG","BOL"), level=0); plot(spdf)

getGADM <- function (fileNames, level = 0, ...) {
  
  ## load the sp-package
  library(sp)
  
  ## Internal function 1 - download data from GADM
  ## filename: Country Codes (e.g. GB = Great Britain, CHN = China)
  ## level: min 0, 1, 2 max
  loadGADM <- function (fileName, level = 0, ...) {
    connection <- url(paste("http://gadm.org/data/rda/", fileName, "_adm", 
                            level, ".RData", sep = ""))
    load(connection)
    close(connection)
    gadm
  }
  
  ## Internal function 2 - assign a prefix
  changeGADMPrefix <- function (GADM, prefix) {
    GADM <- spChFIDs(GADM, paste(prefix, row.names(GADM), sep = "_"))
    GADM
  }
  
  ## Internal function 3 - load file and change prefix
  loadChangePrefix <- function (fileName, level = 0, ...) {
    theFile <- loadGADM(fileName, level)
    theFile <- changeGADMPrefix(theFile, fileName)
    theFile
  }
  
  ## Put everything together
  polygon <- sapply(fileNames, loadChangePrefix, level)
  polyMap <- do.call("rbind", polygon)
  polyMap
  
}

################################################################################